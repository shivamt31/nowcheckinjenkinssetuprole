# Jenkins Setup Role
---------------------

## Introduction
---------------
This role setup jenkins by : 
* Installing jenkins.
* Configure the jenkins system configuration
* Install the jenkins plugin.


### Inventory

```

[jenkinsNodes]
jenkins_server1 ansible_host=192.168.0.130
jenkins_server2 ansible_host=192.168.0.131

[jenkinsNodes:vars]
ansible_user=vagrant

```

### Main Playbook

```

- name: Jenkins setup role
  hosts: jenkinsNodes
  become: yes
  roles:
    - jenkins_setup

```

### Variables

```

install_jenkins_stable: "enabled"                 
jenkins_custom_install: "enabled"               
jenkins_version: "2.222.3"                        
jenkins_port: "8080"

```
